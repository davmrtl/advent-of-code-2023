package main

import (
	"strconv"
	"strings"
)

func part1(input string) int {
	arr := strings.Split(input, "\n")
	datasets := make([][][]int, 0)

	// Parse input
	for _, line := range arr {
		parts := strings.Split(line, " ")
		dataset := make([][]int, 1)
		sequence := make([]int, len(parts))

		for i, part := range parts {
			n, _ := strconv.Atoi(part)
			sequence[i] = n
		}

		dataset[0] = sequence
		datasets = append(datasets, dataset)
	}

	// Process sequences to complete zeros
	sum := 0

	for dataset := range datasets {
		for {
			// Validate sequence sequence of the dataset if contains all zeros
			sequence := len(datasets[dataset]) - 1
			sum := 0

			for _, n := range datasets[dataset][sequence] {
				sum += n
			}

			if sum == 0 {
				break
			}

			// Create new sequence by comparing step between each value
			last := len(datasets[dataset][sequence]) - 1
			new := make([]int, last)
			prev := datasets[dataset][sequence][last]

			for i := last - 1; i >= 0; i-- {
				current := datasets[dataset][sequence][i]
				diff := prev - current

				new[i] = diff

				prev = current
			}

			datasets[dataset] = append(datasets[dataset], new)
		}

		// Add zero to last sequence
		sequence := len(datasets[dataset]) - 1
		datasets[dataset][sequence] = append(datasets[dataset][sequence], 0)

		// Extrapolate next value
		for sequence = sequence - 1; sequence >= 0; sequence-- {
			length := len(datasets[dataset][sequence]) - 1
			current := datasets[dataset][sequence][length]
			prev := datasets[dataset][sequence+1][length]

			new := prev + current

			datasets[dataset][sequence] = append(datasets[dataset][sequence], new)

			if sequence == 0 {
				sum += new
			}
		}
	}

	return sum
}

func part2(input string) int {
	arr := strings.Split(input, "\n")
	datasets := make([][][]int, 0)

	// Parse input
	for _, line := range arr {
		parts := strings.Split(line, " ")
		dataset := make([][]int, 1)
		sequence := make([]int, len(parts))

		for i, part := range parts {
			n, _ := strconv.Atoi(part)
			sequence[i] = n
		}

		dataset[0] = sequence
		datasets = append(datasets, dataset)
	}

	// Process sequences to complete zeros
	sum := 0

	for dataset := range datasets {
		for {
			// Validate sequence sequence of the dataset if contains all zeros
			sequence := len(datasets[dataset]) - 1
			sum := 0

			for _, n := range datasets[dataset][sequence] {
				sum += n
			}

			if sum == 0 {
				break
			}

			// Create new sequence by comparing step between each value
			last := len(datasets[dataset][sequence]) - 1
			new := make([]int, last)
			prev := datasets[dataset][sequence][last]

			for i := last - 1; i >= 0; i-- {
				current := datasets[dataset][sequence][i]
				diff := prev - current

				new[i] = diff

				prev = current
			}

			datasets[dataset] = append(datasets[dataset], new)
		}

		// Add zero to last sequence
		sequence := len(datasets[dataset]) - 1
		datasets[dataset][sequence] = append([]int{0}, datasets[dataset][sequence]...)

		// Extrapolate next value
		for sequence = sequence - 1; sequence >= 0; sequence-- {
			current := datasets[dataset][sequence][0]
			prev := datasets[dataset][sequence+1][0]

			new := current - prev

			datasets[dataset][sequence] = append([]int{new}, datasets[dataset][sequence]...)

			if sequence == 0 {
				sum += new
			}
		}
	}

	return sum
}
