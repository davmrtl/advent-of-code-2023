package main

import (
	"log"
	"os"
	"runtime"
	"strings"
	"testing"
	"time"
)

func fn() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	s := f.Name()
	s = s[strings.LastIndex(s, "/")+1:]
	return s
}

func TestPart1(t *testing.T) {
	lines := `LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)`

	result, expected := part1(lines), 6

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part1(string(input)), 23147

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2(t *testing.T) {
	lines := `LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)`

	result, expected := part2(lines), 6

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part2(string(input)), 22289513667691

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
