package main

import (
	"strings"
	"sync"
)

func part1(input string) int {
	arr := strings.Split(input, "\n")

	index := map[string]int{"L": 0, "R": 1}
	var directions string
	locations := make(map[string][]string)

	for i, line := range arr {
		if i == 0 {
			directions = line

			continue
		}

		if len(line) == 0 {
			continue
		}

		parts := strings.Split(line, "=")
		key := strings.TrimSpace(parts[0])
		parts[1] = strings.ReplaceAll(parts[1], "(", "")
		parts[1] = strings.ReplaceAll(parts[1], ")", "")
		parts[1] = strings.ReplaceAll(parts[1], " ", "")
		parts = strings.Split(parts[1], ",")

		locations[key] = parts
	}

	sum := 0
	direction := 0
	location := "AAA"

	for location != "ZZZ" {
		nexts := locations[location]
		dir := string(directions[direction])
		idx := index[dir]

		location = nexts[idx]
		sum++
		direction++

		if direction >= len(directions) {
			direction = 0
		}
	}

	return sum
}

func gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

func lcm(a, b int, integers ...int) int {
	result := a * b / gcd(a, b)

	for i := 0; i < len(integers); i++ {
		result = lcm(result, integers[i])
	}

	return result
}

func part2(input string) int {
	arr := strings.Split(input, "\n")

	index := map[string]int{"L": 0, "R": 1}
	var directions string
	locations := make(map[string][]string)

	for i, line := range arr {
		if i == 0 {
			directions = line

			continue
		}

		if len(line) == 0 {
			continue
		}

		parts := strings.Split(line, "=")
		key := strings.TrimSpace(parts[0])
		parts[1] = strings.ReplaceAll(parts[1], "(", "")
		parts[1] = strings.ReplaceAll(parts[1], ")", "")
		parts[1] = strings.ReplaceAll(parts[1], " ", "")
		parts = strings.Split(parts[1], ",")

		locations[key] = parts
	}

	// find iter to reach Z
	var wg sync.WaitGroup
	iter := make(chan int, len(locations))
	for location := range locations {
		if !strings.HasSuffix(location, "A") {
			continue
		}

		wg.Add(1)

		go func(location string) {
			defer wg.Done()

			direction := 0
			next := location
			count := 0
			for !strings.HasSuffix(next, "Z") {
				nexts := locations[next]
				dir := string(directions[direction])
				idx := index[dir]

				next = nexts[idx]
				count++
				direction = (direction + 1) % len(directions)
			}

			iter <- count
		}(location)
	}

	wg.Wait()
	close(iter)

	totals := make([]int, 0)
	for total := range iter {
		totals = append(totals, total)
	}

	return lcm(totals[0], totals[1], totals[2:]...)
}
