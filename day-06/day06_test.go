package main

import (
	"os"
	"testing"
)

func TestPart1(t *testing.T) {
	lines := `Time:      7  15   30
Distance:  9  40  200`

	result := part1(lines)

	var expected int = 288
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part1(string(input))

	var expected int = 2756160
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2(t *testing.T) {
	lines := `Time:      7  15   30
Distance:  9  40  200`

	result := part2(lines)

	var expected int = 71503
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part2(string(input))

	var expected int = 34788142
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
