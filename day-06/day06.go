package main

import (
	"strconv"
	"strings"
)

func part1(input string) int {
	arr := strings.Split(input, "\n")

	// Parse input
	times := make([]int, 0)
	records := make([]int, 0)
	for _, line := range arr {
		parts := strings.Split(line, ":")
		var arr *[]int
		if strings.HasPrefix(parts[0], "Time") {
			arr = &times
		} else if strings.HasPrefix(parts[0], "Distance") {
			arr = &records
		}

		parts = strings.Split(parts[1], " ")

		for _, record := range parts {
			if len(record) == 0 {
				continue
			}

			n, _ := strconv.Atoi(record)
			*arr = append(*arr, n)
		}
	}

	// Find best distance for each times
	result := 1
	for i, time := range times {
		record := records[i]
		acc := 0

		for hold := 1; hold < time-1; hold++ {
			distance := (time - hold) * hold
			if distance > record {
				acc++
			}
		}

		result *= acc
	}

	return result
}

func part2(input string) int {
	arr := strings.Split(input, "\n")

	// Parse input
	times := make([]int, 0)
	records := make([]int, 0)
	for _, line := range arr {
		parts := strings.Split(line, ":")
		var arr *[]int
		if strings.HasPrefix(parts[0], "Time") {
			arr = &times
		} else if strings.HasPrefix(parts[0], "Distance") {
			arr = &records
		}

		parts[1] = strings.ReplaceAll(parts[1], " ", "")

		n, _ := strconv.Atoi(parts[1])
		*arr = append(*arr, n)
	}

	// Find best distance for each times
	result := 1
	for i, time := range times {
		record := records[i]
		acc := 0

		for hold := 1; hold < time-1; hold++ {
			distance := (time - hold) * hold
			if distance > record {
				acc++
			}
		}

		result *= acc
	}

	return result
}
