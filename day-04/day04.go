package main

import (
	"slices"
	"strings"
)

func part1(input string) int {
	arr := strings.Split(input, "\n")
	sum := 0

	for _, line := range arr {
		parts := strings.Split(line, ":")
		parts = strings.Split(parts[1], "|")
		winning := strings.Split(parts[0], " ")
		mine := strings.Split(parts[1], " ")

		score := 0
		for _, my := range mine {
			if len(my) == 0 || !slices.Contains(winning, my) {
				continue
			}

			if score == 0 {
				score = 1
			} else {
				score *= 2
			}
		}

		sum += score
	}

	return sum
}

func part2(input string) int {
	arr := strings.Split(input, "\n")
	copy := make([]int, len(arr))

	sum := 0

	for i, line := range arr {
		parts := strings.Split(line, ":")
		parts = strings.Split(parts[1], "|")
		winning := strings.Split(parts[0], " ")
		mine := strings.Split(parts[1], " ")

		copy[i]++

		score := 0
		for _, my := range mine {
			if len(my) == 0 || !slices.Contains(winning, my) {
				continue
			}

			score++
		}

		for j := i + 1; j <= i+score; j++ {
			copy[j] += copy[i]
		}

		sum += copy[i]
	}

	return sum
}
