package main

import (
	"log"
	"os"
	"runtime"
	"strings"
	"testing"
	"time"
)

func fn() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	s := f.Name()
	s = s[strings.LastIndex(s, "/")+1:]
	return s
}

func TestPart1a(t *testing.T) {
	lines := `.....
.S-7.
.|.|.
.L-J.
.....`

	result, expected := part1(lines), 4

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1b(t *testing.T) {
	lines := `..F7.
.FJ|.
SJ.L7
|F--J
LJ...`

	result, expected := part1(lines), 8

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part1(string(input)), 6886

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2a(t *testing.T) {
	lines := `...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........`

	result, expected := part2(lines), 4

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2b(t *testing.T) {
	lines := `FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L`

	result, expected := part2(lines), 10

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part2(string(input)), 371

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
