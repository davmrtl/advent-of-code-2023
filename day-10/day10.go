package main

import (
	"slices"
	"strings"
)

func part1(input string) int {
	type point struct {
		x int
		y int
	}
	type path struct {
		current point
		prev    []point
	}
	nsew := map[string]func(point) point{
		"N": func(p point) point {
			return point{
				x: p.x,
				y: p.y - 1,
			}
		},
		"S": func(p point) point {
			return point{
				x: p.x,
				y: p.y + 1,
			}
		},
		"E": func(p point) point {
			return point{
				x: p.x + 1,
				y: p.y,
			}
		},
		"W": func(p point) point {
			return point{
				x: p.x - 1,
				y: p.y,
			}
		},
	}
	nexts := map[string]func(point) []point{
		"|": func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["S"](p),
			}
		},
		"-": func(p point) []point {
			return []point{
				nsew["E"](p),
				nsew["W"](p),
			}
		},
		"L": func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["E"](p),
			}
		},
		"J": func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["W"](p),
			}
		},
		"7": func(p point) []point {
			return []point{
				nsew["S"](p),
				nsew["W"](p),
			}
		},
		"F": func(p point) []point {
			return []point{
				nsew["S"](p),
				nsew["E"](p),
			}
		},
		"S": func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["S"](p),
				nsew["E"](p),
				nsew["W"](p),
			}
		},
	}

	arr := strings.Split(input, "\n")
	var maze []string
	var visit []path
	var start *point

	// Parse input
	for y, line := range arr {
		maze = append(maze, line)

		if start != nil {
			continue
		}

		// Find S
		if x := strings.Index(line, "S"); x != -1 {
			s := point{
				x: x,
				y: y,
			}
			start = &s

			prev := []point{s}

			for _, next := range nexts["S"](s) {
				visit = append(visit, path{
					current: next,
					prev:    prev,
				})
			}
		}
	}

	var paths []path
	for len(visit) > 0 {
		pop := visit[0]
		visit = visit[1:]

		// Check if current is start
		if start.x == pop.current.x && start.y == pop.current.y {
			paths = append(paths, pop)
			continue
		}

		if pop.current.x < 0 || pop.current.y < 0 {
			continue
		}

		// Check if the last connects with current
		last := pop.prev[len(pop.prev)-1]
		current := string(maze[pop.current.y][pop.current.x])

		var move bool
		var nextfn func(point) []point
		if nextfn, move = nexts[current]; !move {
			continue
		}

		nexts := nextfn(pop.current)

		var lastIndex *int
		for i, next := range nexts {
			if next.x == last.x && next.y == last.y {
				lastIndex = &i
				break
			}
		}

		if lastIndex == nil {
			continue
		}

		// Enqueue nexts that arent last
		for i, next := range nexts {
			if i == *lastIndex {
				continue
			}

			prev := append(pop.prev, pop.current)
			visit = append(visit, path{
				current: next,
				prev:    prev,
			})
		}
	}

	for _, path := range paths {
		return len(path.prev) / 2
	}

	return 0
}

func part2(input string) int {
	type point struct {
		x int
		y int
	}
	type path struct {
		current point
		prev    []point
	}
	nsew := map[string]func(point) point{
		"N": func(p point) point {
			return point{
				x: p.x,
				y: p.y - 1,
			}
		},
		"S": func(p point) point {
			return point{
				x: p.x,
				y: p.y + 1,
			}
		},
		"E": func(p point) point {
			return point{
				x: p.x + 1,
				y: p.y,
			}
		},
		"W": func(p point) point {
			return point{
				x: p.x - 1,
				y: p.y,
			}
		},
	}
	nexts := map[rune]func(point) []point{
		'|': func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["S"](p),
			}
		},
		'-': func(p point) []point {
			return []point{
				nsew["E"](p),
				nsew["W"](p),
			}
		},
		'L': func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["E"](p),
			}
		},
		'J': func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["W"](p),
			}
		},
		'7': func(p point) []point {
			return []point{
				nsew["S"](p),
				nsew["W"](p),
			}
		},
		'F': func(p point) []point {
			return []point{
				nsew["S"](p),
				nsew["E"](p),
			}
		},
		'S': func(p point) []point {
			return []point{
				nsew["N"](p),
				nsew["S"](p),
				nsew["E"](p),
				nsew["W"](p),
			}
		},
	}

	arr := strings.Split(input, "\n")
	var maze []string
	var visit []path
	var start *point

	// Parse input
	for y, line := range arr {
		maze = append(maze, line)

		if start != nil {
			continue
		}

		// Find S
		if x := strings.Index(line, "S"); x != -1 {
			s := point{
				x: x,
				y: y,
			}
			start = &s

			prev := []point{s}

			for _, next := range nexts['S'](s) {
				visit = append(visit, path{
					current: next,
					prev:    prev,
				})
			}
		}
	}

	var paths []path
	for len(visit) > 0 {
		pop := visit[0]
		visit = visit[1:]

		// Check if current is start
		if start.x == pop.current.x && start.y == pop.current.y {
			paths = append(paths, pop)
			continue
		}

		if pop.current.x < 0 || pop.current.y < 0 {
			continue
		}

		// Check if the last connects with current
		last := pop.prev[len(pop.prev)-1]
		current := rune(maze[pop.current.y][pop.current.x])

		var move bool
		var nextfn func(point) []point
		if nextfn, move = nexts[current]; !move {
			continue
		}

		nexts := nextfn(pop.current)

		var lastIndex *int
		for i, next := range nexts {
			if next.x == last.x && next.y == last.y {
				lastIndex = &i
				break
			}
		}

		if lastIndex == nil {
			continue
		}

		// Enqueue nexts that arent last
		for i, next := range nexts {
			if i == *lastIndex {
				continue
			}

			prev := append(pop.prev, pop.current)
			visit = append(visit, path{
				current: next,
				prev:    prev,
			})
		}
	}

	// Convert S
	main := paths[0].prev
	first := main[1]
	last := main[len(main)-1]
	var replace rune

	for key, fn := range nexts {
		temp := fn(*start)

		if key != 'S' && slices.Contains(temp, first) && slices.Contains(temp, last) {
			replace = key
			break
		}
	}

	for y, row := range maze {
		for x := range row {
			p := point{
				x: x,
				y: y,
			}

			if !slices.Contains(main, p) {
				maze[y] = row[:x] + "." + row[x+1:]
			}
		}
	}

	// Loop on paths to count enclosed tiles
	count := 0
	var hits []point
	var parity int
	for y, row := range maze {
		parity = 0
		var knot *rune
		for x, tile := range row {
			p := point{
				x: x,
				y: y,
			}

			exists := slices.Contains(main, p)

			if tile == 'S' {
				tile = replace
			}

			if exists {
				switch tile {
				case '|':
					parity++
				case 'S', 'L', 'F':
					t := tile
					knot = &t
				case 'J':
					if *knot == 'F' {
						parity++
						knot = nil
					}
				case '7':
					if *knot == 'L' {
						parity++
						knot = nil
					}

				}

				continue
			}

			if parity%2 == 1 {
				count++
				hits = append(hits, point{
					x: x,
					y: y,
				})
			}

			// Check if tile is openning/closing path

		}
	}

	// // Debug
	// reset := "\033[0m"
	// red := "\033[31m"
	// fmt.Println()
	// for y, line := range maze {
	// 	for x, s := range line {
	// 		str := string(s)
	// 		for _, hit := range hits {
	// 			if hit.x == x && hit.y == y {
	// 				str = red + str + reset
	// 				break
	// 			}
	// 		}
	// 		fmt.Print(str)
	// 	}
	// 	fmt.Println()
	// }

	return count
}
