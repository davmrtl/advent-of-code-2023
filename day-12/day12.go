package main

import (
	"fmt"
	"strings"
)

func part1(input string) int {
	arr := strings.Split(input, "\n")
	sum := 0

	var matches = func(puzzle string, pattern string) bool {
		var resolved string

		acc := 0
		for i := range puzzle {
			if puzzle[i] == '.' {
				if acc > 0 {
					if len(resolved) > 0 {
						resolved += ","
					}
					resolved += fmt.Sprintf("%d", acc)
					acc = 0
				}
				continue
			}
			if puzzle[i] == '#' {
				acc++
			}
		}

		if acc > 0 {
			if len(resolved) > 0 {
				resolved += ","
			}
			resolved += fmt.Sprintf("%d", acc)
		}

		return pattern == resolved
	}

	// Parse input
	for _, line := range arr {
		parts := strings.Split(line, " ")
		puzzle := parts[0]
		pattern := parts[1]
		stack := []string{puzzle}

		for len(stack) > 0 {
			// pop stack
			pop := stack[0]
			stack = stack[1:]

			idx := strings.Index(pop, "?")
			if idx == -1 {
				if matches(pop, pattern) {
					sum++
				}

				continue
			}

			stack = append(stack, pop[:idx]+"."+pop[idx+1:], pop[:idx]+"#"+pop[idx+1:])
		}
	}

	return sum
}
