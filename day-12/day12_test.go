package main

import (
	"log"
	"os"
	"runtime"
	"strings"
	"testing"
	"time"
)

func fn() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	s := f.Name()
	s = s[strings.LastIndex(s, "/")+1:]
	return s
}

func TestPart1(t *testing.T) {
	lines := `???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1`

	result, expected := part1(lines), 21

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part1(string(input)), 7025

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
