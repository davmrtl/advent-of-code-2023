package main

import (
	"os"
	"testing"
)

func TestPart1(t *testing.T) {
	lines := `1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet`

	result := part1(lines)

	if result != 142 {
		t.Fatalf("Expected 142, but got %d", result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part1(string(input))

	if result != 54667 {
		t.Fatalf("Expected 54667, but got %d", result)
	}
}

func TestPart2(t *testing.T) {
	lines := `two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen`

	result := part2(lines)

	if result != 281 {
		t.Fatalf("Expected 281, but got %d", result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part2(string(input))

	if result != 54203 {
		t.Fatalf("Expected 54203, but got %d", result)
	}
}
