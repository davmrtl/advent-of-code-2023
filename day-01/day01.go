package main

import (
	"strconv"
	"strings"
)

var words = []string{
	"1", "2", "3", "4", "5", "6", "7", "8", "9",

	"one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
}

func part1(input string) int {
	return process(input, words[:9]...)
}

func part2(input string) int {
	return process(input, words...)
}

func process(input string, words ...string) int {
	arr := strings.Split(input, "\n")

	sum := 0
	for _, line := range arr {
		lowest := -1
		first := -1
		highest := -1
		last := -1
		for i, word := range words {
			min := strings.Index(line, word)
			max := strings.LastIndex(line, word)

			if min >= 0 && (lowest == -1 || min < lowest) {
				lowest = min
				first = i
			}

			if max >= 0 && (highest == -1 || max > highest) {
				highest = max
				last = i
			}
		}

		// // Debug line
		// reset := "\033[0m"
		// red := "\033[31m"
		// firstWord := words[first]
		// lastWord := words[last]

		// if lowest != highest {
		// 	line = line[:lowest] + red + firstWord + reset + line[lowest+len(firstWord):highest] + red + lastWord + reset + line[highest+len(lastWord):]
		// } else {
		// 	line = line[:lowest] + red + firstWord + reset + line[lowest+len(firstWord):]
		// }

		// fmt.Printf("%s\t%s%s\n", line, firstWord, lastWord)
		// time.Sleep(500 * time.Millisecond)

		num, _ := strconv.Atoi(words[first%9] + words[last%9])
		sum += num
	}

	return sum
}
