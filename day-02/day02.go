package main

import (
	"strconv"
	"strings"
)

var limit = map[string]int{"red": 12, "green": 13, "blue": 14}

func part1(input string) int {
	arr := strings.Split(input, "\n")

	sum := 0
	for _, line := range arr {
		parts := strings.Split(line, ":")
		game, _ := strconv.Atoi(strings.Split(parts[0], " ")[1])
		valid := true

		for _, reveal := range strings.Split(parts[1], ";") {
			balls := map[string]int{"red": 0, "green": 0, "blue": 0}

			for _, ball := range strings.Split(reveal, ",") {
				parts = strings.Split(strings.TrimSpace(ball), " ")
				n, _ := strconv.Atoi(parts[0])

				balls[parts[1]] += n
			}

			for ball, max := range limit {
				valid = valid && balls[ball] <= max
			}

			if !valid {
				break
			}
		}

		if valid {
			sum += game
		}
	}

	return sum
}

func part2(input string) int {
	arr := strings.Split(input, "\n")

	sum := 0
	for _, line := range arr {
		parts := strings.Split(line, ":")
		maxs := map[string]int{"red": 0, "green": 0, "blue": 0}

		for _, reveal := range strings.Split(parts[1], ";") {
			for _, ball := range strings.Split(reveal, ",") {
				parts = strings.Split(strings.TrimSpace(ball), " ")
				n, _ := strconv.Atoi(parts[0])

				if n > maxs[parts[1]] {
					maxs[parts[1]] = n
				}
			}
		}

		power := 1
		for _, min := range maxs {
			power *= min
		}

		sum += power
	}

	return sum
}
