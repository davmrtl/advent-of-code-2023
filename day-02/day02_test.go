package main

import (
	"os"
	"testing"
)

func TestPart1(t *testing.T) {
	lines := `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`

	result := part1(lines)

	if result != 8 {
		t.Fatalf("Expected 8, but got %d", result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part1(string(input))

	if result != 2795 {
		t.Fatalf("Expected 2795, but got %d", result)
	}
}

func TestPart2(t *testing.T) {
	lines := `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`

	result := part2(lines)

	if result != 2286 {
		t.Fatalf("Expected 2286, but got %d", result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part2(string(input))

	if result != 75561 {
		t.Fatalf("Expected 75561, but got %d", result)
	}
}
