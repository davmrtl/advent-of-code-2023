package main

import (
	"os"
	"testing"
)

func TestPart1(t *testing.T) {
	lines := `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`

	result := part1(lines)

	var expected int = 6440
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part1(string(input))

	var expected int = 250602641
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2(t *testing.T) {
	lines := `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`

	result := part2(lines)

	var expected int = 5905
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part2(string(input))

	var expected int = 251037509
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
