package main

import (
	"sort"
	"strconv"
	"strings"
)

func index(cards string, order string, jokers bool) int {
	count := count(cards, order)

	// jokers
	j := strings.Index(order, "J")
	js := count[j]

	if jokers && js > 0 {
		card := 0
		max := -1
		for i := 0; i < len(order)-1; i++ {
			if max == -1 || count[i] > max {
				card = i
				max = count[i]
			}
		}

		count[card] += js
		count[j] = 0
	}

	sort.Ints(count)
	last := count[len(count)-1]
	surlast := count[len(count)-2]

	switch last {
	case 5:
		return 0 // Five of a kind
	case 4:
		return 1 // Four of a kind
	case 3:
		if surlast == 2 {
			return 2 // Full house
		}
		return 3 // Three of a kind
	case 2:
		if surlast == 2 {
			return 4 // Two pair
		}
		return 5 // One pair
	default:
		return 6 // High card
	}
}

func count(cards string, order string) []int {
	count := make([]int, len(order))

	for _, c := range cards {
		idx := strings.Index(order, string(c))
		count[idx]++
	}

	return count
}

type hand struct {
	cards string
	bid   int
	index int
}

func part1(input string) int {
	order := "AKQJT98765432"
	arr := strings.Split(input, "\n")
	hands := make([]hand, len(arr))

	for i, line := range arr {
		parts := strings.Split(line, " ")
		cards := parts[0]
		bid, _ := strconv.Atoi(parts[1])
		idx := index(cards, order, false)

		hands[i] = hand{
			cards: cards,
			bid:   bid,
			index: idx,
		}
	}

	// Sort by type then by cards order
	sort.SliceStable(hands, func(h1, h2 int) bool {
		hand1 := hands[h1]
		hand2 := hands[h2]

		// Compare hand order
		if hand1.index < hand2.index {
			return true
		} else if hand1.index > hand2.index {
			return false
		}

		// Compare cards order
		for i, c1 := range hand1.cards {
			c2 := hand2.cards[i]
			card1 := strings.Index(order, string(c1))
			card2 := strings.Index(order, string(c2))

			if card1 < card2 {
				return true
			} else if card1 > card2 {
				return false
			}
		}

		return false
	})

	sum := 0
	for i, hand := range hands {
		sum += hand.bid * (len(hands) - i)
	}

	return sum
}

func part2(input string) int {
	order := "AKQT98765432J"
	arr := strings.Split(input, "\n")
	hands := make([]hand, len(arr))

	for i, line := range arr {
		parts := strings.Split(line, " ")
		cards := parts[0]
		bid, _ := strconv.Atoi(parts[1])
		idx := index(cards, order, true)

		hands[i] = hand{
			cards: cards,
			bid:   bid,
			index: idx,
		}
	}

	// Sort by type then by cards order
	sort.SliceStable(hands, func(h1, h2 int) bool {
		hand1 := hands[h1]
		hand2 := hands[h2]

		// Compare hand order
		if hand1.index < hand2.index {
			return true
		} else if hand1.index > hand2.index {
			return false
		}

		// Compare cards order
		for i, c1 := range hand1.cards {
			c2 := hand2.cards[i]
			card1 := strings.Index(order, string(c1))
			card2 := strings.Index(order, string(c2))

			if card1 < card2 {
				return true
			} else if card1 > card2 {
				return false
			}
		}

		return false
	})

	sum := 0
	for i, hand := range hands {
		sum += hand.bid * (len(hands) - i)
	}

	return sum
}
