package main

import (
	"os"
	"testing"
)

func TestPart1(t *testing.T) {
	lines := `467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..`

	result := part1(lines)

	expected := 4361
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part1(string(input))

	expected := 540131
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2(t *testing.T) {
	lines := `467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..`

	result := part2(lines)

	expected := 467835
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part2(string(input))

	expected := 86879020
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
