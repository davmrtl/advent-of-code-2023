package main

import (
	"strconv"
	"strings"
)

type rectangle struct {
	x        int
	y        int
	width    int
	height   int
	value    string
	collided bool
}

func part1(input string) int {
	arr := strings.Split(input, "\n")

	// Index parts and symbols
	parts := make([]*rectangle, 0)
	symbols := make([]*rectangle, 0)

	for y, line := range arr {
		var part *rectangle

		for x, c := range line {
			char := string(c)

			if char == "." && part != nil {
				parts = append(parts, part)
				part = nil
			} else if strings.ContainsAny(char, "-@*/&#%+=$") {
				if part != nil {
					parts = append(parts, part)
					part = nil
				}

				symbols = append(symbols, &rectangle{
					x:      x - 1,
					y:      y - 1,
					width:  2,
					height: 2,
					value:  char,
				})
			} else if strings.ContainsAny(char, "1234567890") {
				if part == nil {
					part = &rectangle{
						x:      x,
						y:      y,
						width:  0,
						height: 0,
						value:  char,
					}
				} else {
					part.width++
					part.value += char
				}
			}
		}

		if part != nil {
			parts = append(parts, part)
			part = nil
		}
	}

	// Find parts near symbols
	sum := 0

	for _, part := range parts {
		for _, symbol := range symbols {
			if symbol.x+symbol.width >= part.x &&
				symbol.x <= part.x+part.width &&
				symbol.y+symbol.height >= part.y &&
				symbol.y <= part.y+part.height {
				value, _ := strconv.Atoi(part.value)
				sum += value
				part.collided = true
				break
			}
		}
	}

	// // Debug schema
	// reset := "\033[0m"
	// red := "\033[31m"
	// green := "\033[42m"
	// blue := "\033[34m"
	// for y, line := range arr {
	// 	for x, c := range line {
	// 		char := string(c)
	// 		var color *string

	// 		for _, part := range parts {
	// 			if y >= part.y && y <= part.y+part.height &&
	// 				x >= part.x && x <= part.x+part.width {
	// 				color = &red
	// 				if part.collided {
	// 					color = &green
	// 				}
	// 				break
	// 			}
	// 		}

	// 		for _, symbol := range symbols {
	// 			if y >= symbol.y && y <= symbol.y+symbol.height &&
	// 				x >= symbol.x && x <= symbol.x+symbol.width {
	// 				color = &blue
	// 				break
	// 			}
	// 		}

	// 		if color != nil {
	// 			char = *color + char + reset
	// 		}

	// 		fmt.Print(char)
	// 	}

	// 	fmt.Println()
	// }

	return sum
}

func part2(input string) int {
	arr := strings.Split(input, "\n")

	// Index parts and symbols
	parts := make([]*rectangle, 0)
	symbols := make([]*rectangle, 0)

	for y, line := range arr {
		var part *rectangle

		for x, c := range line {
			char := string(c)

			if char == "." && part != nil {
				parts = append(parts, part)
				part = nil
			} else if strings.ContainsAny(char, "*") {
				if part != nil {
					parts = append(parts, part)
					part = nil
				}

				symbols = append(symbols, &rectangle{
					x:      x - 1,
					y:      y - 1,
					width:  2,
					height: 2,
					value:  char,
				})
			} else if strings.ContainsAny(char, "1234567890") {
				if part == nil {
					part = &rectangle{
						x:      x,
						y:      y,
						width:  0,
						height: 0,
						value:  char,
					}
				} else {
					part.width++
					part.value += char
				}
			}
		}

		if part != nil {
			parts = append(parts, part)
			part = nil
		}
	}

	// Find parts near symbols
	sum := 0

	for _, symbol := range symbols {
		near := make([]*rectangle, 0)
		for _, part := range parts {
			if symbol.x+symbol.width >= part.x &&
				symbol.x <= part.x+part.width &&
				symbol.y+symbol.height >= part.y &&
				symbol.y <= part.y+part.height {
				near = append(near, part)
			}
		}

		if len(near) != 2 {
			continue
		}

		ratio := 1
		for _, part := range near {
			part.collided = true
			value, _ := strconv.Atoi(part.value)
			ratio *= value
		}

		sum += ratio
	}

	// // Debug schema
	// reset := "\033[0m"
	// red := "\033[31m"
	// green := "\033[42m"
	// blue := "\033[34m"
	// for y, line := range arr {
	// 	for x, c := range line {
	// 		char := string(c)
	// 		var color *string

	// 		for _, part := range parts {
	// 			if y >= part.y && y <= part.y+part.height &&
	// 				x >= part.x && x <= part.x+part.width {
	// 				color = &red
	// 				if part.collided {
	// 					color = &green
	// 				}
	// 				break
	// 			}
	// 		}

	// 		for _, symbol := range symbols {
	// 			if y >= symbol.y && y <= symbol.y+symbol.height &&
	// 				x >= symbol.x && x <= symbol.x+symbol.width {
	// 				color = &blue
	// 				break
	// 			}
	// 		}

	// 		if color != nil {
	// 			char = *color + char + reset
	// 		}

	// 		fmt.Print(char)
	// 	}

	// 	fmt.Println()
	// }

	return sum
}
