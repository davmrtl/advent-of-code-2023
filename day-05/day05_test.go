package main

import (
	"log"
	"os"
	"testing"
	"time"
)

func TestPart1(t *testing.T) {
	lines := `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`

	result := part1(lines)

	var expected uint = 35
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")
	result := part1(string(input))

	var expected uint = 31599214
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2(t *testing.T) {
	lines := `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`

	result := part2(lines)

	var expected uint = 46
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()
	result := part2(string(input))
	log.Printf("took %s", time.Since(start))

	var expected uint = 20358599
	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
