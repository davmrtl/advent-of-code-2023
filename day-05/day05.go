package main

import (
	"math"
	"strconv"
	"strings"
	"sync"
)

type pair struct {
	seed   uint
	length uint
}

type mapping struct {
	destination uint
	source      uint
	length      uint
}

func (m *mapping) check(n uint) (bool, uint) {
	min := m.source
	max := m.source + m.length - 1
	diff := m.destination - m.source

	if n >= min && n <= max {
		return true, n + diff
	}

	return false, 0
}

func part1(input string) uint {
	arr := strings.Split(input, "\n")
	var mappings = make(map[string][]mapping)
	var seeds = make([]uint, 0)
	var current string

	// Index things
	for _, line := range arr {
		if len(line) == 0 {
			continue
		}

		if strings.HasPrefix(line, "seeds") {
			parts := strings.Split(line, ":")
			parts = strings.Split(parts[1], " ")

			for _, seed := range parts {
				if len(seed) == 0 {
					continue
				}
				n, _ := strconv.Atoi(seed)
				seeds = append(seeds, uint(n))
			}

			continue
		}

		if strings.Contains(line, "map:") {
			current = strings.Split(line, " ")[0]

			continue
		}

		if _, exists := mappings[current]; !exists {
			mappings[current] = make([]mapping, 0)
		}

		parts := strings.Split(line, " ")
		destination, _ := strconv.Atoi(parts[0])
		source, _ := strconv.Atoi(parts[1])
		length, _ := strconv.Atoi(parts[2])

		mappings[current] = append(mappings[current], mapping{
			destination: uint(destination),
			source:      uint(source),
			length:      uint(length),
		})
	}

	// Start from seed up to location
	var min *uint
	for _, seed := range seeds {
		next := seed

		for _, m := range []string{
			"seed-to-soil",
			"soil-to-fertilizer",
			"fertilizer-to-water",
			"water-to-light",
			"light-to-temperature",
			"temperature-to-humidity",
			"humidity-to-location"} {

			for _, mapping := range mappings[m] {
				if match, n := mapping.check(next); match {
					next = n
					break
				}
			}
		}

		if min == nil || next < *min {
			min = &next
		}
	}

	return *min
}

func part2(input string) uint {
	arr := strings.Split(input, "\n")
	var mappings = make(map[string][]mapping)
	var seeds = make([]pair, 0)
	var current string

	// Index things
	for _, line := range arr {
		if len(line) == 0 {
			continue
		}

		if strings.HasPrefix(line, "seeds") {
			parts := strings.Split(line, ":")
			parts = strings.Split(parts[1], " ")

			var seed *pair

			for _, part := range parts {
				if len(part) == 0 {
					continue
				}

				n, _ := strconv.Atoi(part)

				if seed == nil {
					seed = &pair{
						seed: uint(n),
					}
					continue
				}

				seed.length = uint(n)
				seeds = append(seeds, *seed)

				seed = nil
			}

			continue
		}

		if strings.Contains(line, "map:") {
			current = strings.Split(line, " ")[0]

			continue
		}

		if _, exists := mappings[current]; !exists {
			mappings[current] = make([]mapping, 0)
		}

		parts := strings.Split(line, " ")
		destination, _ := strconv.Atoi(parts[0])
		source, _ := strconv.Atoi(parts[1])
		length, _ := strconv.Atoi(parts[2])

		mappings[current] = append(mappings[current], mapping{
			destination: uint(destination),
			source:      uint(source),
			length:      uint(length),
		})
	}

	// Start from seed up to location
	locations := make(chan uint, 1000)
	var wg sync.WaitGroup
	for _, seed := range seeds {
		chunk := uint(math.Ceil(float64(seed.length) / 5))
		chunks := make([]pair, 5)

		for i := range chunks {
			chunks[i] = pair{
				seed:   seed.seed + (chunk * uint(i)),
				length: chunk,
			}

			if seed.seed+seed.length < chunks[i].seed+chunks[i].length {
				chunks[i].length = seed.seed + seed.length - chunks[i].seed
			}
		}

		for _, seed := range chunks {
			wg.Add(1)
			go func(seed pair) {
				defer wg.Done()

				var min *uint

				for i := seed.seed; i < seed.seed+seed.length; i++ {
					next := i

					for _, m := range []string{
						"seed-to-soil",
						"soil-to-fertilizer",
						"fertilizer-to-water",
						"water-to-light",
						"light-to-temperature",
						"temperature-to-humidity",
						"humidity-to-location"} {

						for _, mapping := range mappings[m] {
							if match, n := mapping.check(next); match {
								next = n
								break
							}
						}
					}

					if min == nil || next < *min {
						t := next
						min = &t
					}
				}

				locations <- *min
			}(seed)
		}
	}

	wg.Wait()

	close(locations)

	var min *uint
	for next := range locations {
		if min == nil || next < *min {
			t := next
			min = &t

		}
	}

	return *min
}
