package main

import (
	"math"
	"strings"
)

func part1(input string) int {
	type point struct {
		x int
		y int
	}

	arr := strings.Split(input, "\n")
	var galaxies []point
	rows := make(map[int]bool)
	cols := make(map[int]bool)

	// Parse input
	for y, line := range arr {
		for x, g := range line {
			if g != '#' {
				continue
			}

			rows[y] = true
			cols[x] = true
			galaxies = append(galaxies, point{
				x: x,
				y: y,
			})
		}
	}

	// Calculate min distance for each galaxies
	sum := 0
	for i, galaxy := range galaxies {
		// Calculate min distance
		for j, next := range galaxies {
			if j <= i {
				continue
			}

			distance := math.Abs(float64(next.x)-float64(galaxy.x)) + math.Abs(float64(next.y)-float64(galaxy.y))

			// Add expansion cols
			from := galaxy.x
			to := next.x
			if to < from {
				t := from
				from = to
				to = t
			}

			for col := from + 1; col < to; col++ {
				if cols[col] {
					continue
				}

				distance++
			}

			// Add expansion rows
			from = galaxy.y
			to = next.y
			if to < from {
				t := from
				from = to
				to = t
			}

			for row := from + 1; row < to; row++ {
				if rows[row] {
					continue
				}

				distance++
			}

			sum += int(distance)
		}
	}

	return sum
}

func part2(input string, expansion int) int {
	type point struct {
		x int
		y int
	}

	arr := strings.Split(input, "\n")
	var galaxies []point
	rows := make(map[int]bool)
	cols := make(map[int]bool)

	// Parse input
	for y, line := range arr {
		for x, g := range line {
			if g != '#' {
				continue
			}

			rows[y] = true
			cols[x] = true
			galaxies = append(galaxies, point{
				x: x,
				y: y,
			})
		}
	}

	// Calculate min distance for each galaxies
	sum := 0
	for i, galaxy := range galaxies {
		// Calculate min distance
		for j, next := range galaxies {
			if j <= i {
				continue
			}

			distance := math.Abs(float64(next.x)-float64(galaxy.x)) + math.Abs(float64(next.y)-float64(galaxy.y))

			// Add expansion cols
			from := galaxy.x
			to := next.x
			if to < from {
				t := from
				from = to
				to = t
			}

			for col := from + 1; col < to; col++ {
				if cols[col] {
					continue
				}

				distance += float64(expansion)
			}

			// Add expansion rows
			from = galaxy.y
			to = next.y
			if to < from {
				t := from
				from = to
				to = t
			}

			for row := from + 1; row < to; row++ {
				if rows[row] {
					continue
				}

				distance += float64(expansion)
			}

			sum += int(distance)
		}
	}

	return sum
}
