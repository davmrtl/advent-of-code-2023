package main

import (
	"log"
	"os"
	"runtime"
	"strings"
	"testing"
	"time"
)

func fn() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	s := f.Name()
	s = s[strings.LastIndex(s, "/")+1:]
	return s
}

func TestPart1(t *testing.T) {
	lines := `...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....`

	result, expected := part1(lines), 374

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart1Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part1(string(input)), 9521776

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2(t *testing.T) {
	lines := `...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....`

	result, expected := part2(lines, 9), 1030

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}

func TestPart2Final(t *testing.T) {
	input, _ := os.ReadFile("input.txt")

	start := time.Now()

	result, expected := part2(string(input), 999_999), 0

	log.Printf("%s\t%s\n", fn(), time.Since(start))

	if result != expected {
		t.Fatalf("Expected %d, but got %d", expected, result)
	}
}
